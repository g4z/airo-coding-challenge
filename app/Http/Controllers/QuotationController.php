<?php

namespace App\Http\Controllers;

use DateTime;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Requests\QuotationRequest;

class QuotationController extends Controller
{
    /**
     * @var int $fixedRate The fixed rate
     */
    private int $fixedRate = 3;

    /**
     * @var array $ageLoads Table of age-based rates
     *
     * Format is [min-age, max-age, rate]
     */
    private array $ageLoads = [
        [18, 30, 0.6],
        [31, 40, 0.7],
        [41, 50, 0.8],
        [51, 60, 0.9],
        [61, 70, 1.0],
    ];

    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Get the quote
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getQuote(QuotationRequest $request)
    {
        // Generate a uniq ID for this quote request
        $quotationId = Str::uuid();

        // parse ages from string and convert to integers
        $ages = collect(explode(',', request('age')))
            ->map(function ($item, $key) {
                return intval($item);
            })
            ->toArray();

        // Function to get rate for a specific age
        $getRate = function(int $age) : float {
            foreach ($this->ageLoads as $load) {
                if ($age >= $load[0] && $age <= $load[1]) {
                    return $load[2];
                }
            }
            abort(500, 'Error processing age inputs');
        };

        // compute the total number of days
        $days = (new DateTime(request('start_date')))
            ->diff(new DateTime(request('end_date')))
            ->days;

        $days++; // inclusive

        // compute the total
        $total = 0.0;
        foreach ($ages as $age) {
            $total += (3 * $getRate($age) * $days);
        }

        return response()->json([
            'total' => number_format($total, 2),
            'currency_id' => request('currency_id'),
            'quotation_id' => $quotationId,
        ]);
    }
}
