<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Custom validation for age input
        // TODO move this to ValidationServiceProvider
        Validator::extend('age', function($attribute, $value) {
            foreach (explode(',', $value) as $age) {
                $intval = intval($age);
                if ($intval < 18 || $intval > 70) {
                    return false;
                }
            }
            return true;
        });
    }
}
